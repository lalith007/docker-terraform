## Usage

Create a script called `terraform` that looks like:

    #!/bin/sh
    docker run --rm -it -v $PWD:/mnt/workdir registry.gitlab.com/tvaughan/docker-terraform:latest terraform "$@"

make it executable, and then run it.
