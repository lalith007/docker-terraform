FROM registry.gitlab.com/tvaughan/docker-ubuntu:16.04
MAINTAINER "Tom Vaughan <tvaughan@tocino.cl>"

RUN curl -sL https://releases.hashicorp.com/terraform/0.10.2/terraform_0.10.2_linux_amd64.zip   \
        -o terraform.zip                                                                        \
    && unzip -d /usr/local/bin terraform.zip                                                    \
    && chmod a+x /usr/local/bin/terraform*                                                      \
    && rm -f terraform.zip

CMD ["terraform"]
