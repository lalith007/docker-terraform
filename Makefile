REGISTRY = registry.gitlab.com
CONTAINER = tvaughan/docker-terraform
VERSION = latest

.PHONY: all build is-defined-% login push

all: build

is-defined-%:
	@$(if $(value $*),,$(error The environment variable $* is undefined))

build:
	@docker build -t $(REGISTRY)/$(CONTAINER):$(VERSION) .

login: is-defined-GITLAB_USERNAME is-defined-GITLAB_PASSWORD
	@docker login -u $(GITLAB_USERNAME) -p "$(GITLAB_PASSWORD)" $(REGISTRY)

push: build login
	@docker push $(REGISTRY)/$(CONTAINER):$(VERSION)
